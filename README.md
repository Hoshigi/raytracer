# Features #
* Reflections
* Refractions (Transparency)
* Depth of Field 

## Raytracer final result with Depth of Field - 64 Samples ##

![Wynik -final.png](https://bitbucket.org/repo/RqdM9K/images/2604654889-Wynik%20-final.png)