#include "FreeImage.h"
#ifndef _CIMAGE_H_
#define _CIMAGE_H_

/** Klasa reprezentujaca obraz wyjsciowy
*/
class CImage {
private:
	
	float* data; /**< dane obrazu */
	int width; /**< liczba pikseli w poziomie */
	int height; /**< liczba pikseli w pionie */
public:
	FIBITMAP * bitmap;

	/** Zapisanie obrazu w pliku dyskowym
	*	@param fname - nazwa pliku
	*/
	CImage() {};
	CImage(int width, int height);
	void save(char* fname);

};

#endif