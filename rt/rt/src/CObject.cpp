#include "stdafx.h"



	 float CObject::intersect(CRay* ray) {
		return -10;
	}
	 glm::vec3 CObject::get_normal(glm::vec3 intersect) {
		 return glm::vec3(0.0f, 0.0f, 0.0f);
	 }
	 void CObject::flip_normal() {};
	 CSphere::CSphere(float s_r, glm::vec3 s_p, glm::vec3 s_amb, glm::vec3 s_diff, glm::vec3 s_spec, float shine,float refle,float refraindex)
	 {
		 type = OBJ_SPHERE;
		 r=s_r;
		 p = s_p;
		 ambient= s_amb;
		 diffuse = s_diff;
		 specular = s_spec;
		 shininess = shine;
		 reflect = refle;
		 refractionindex = refraindex;
	 }
	 glm::vec3 CSphere::get_normal(glm::vec3 intersect) {
		 return glm::normalize( intersect-p);
	 }
	 float CSphere::intersect(CRay* ray) {
		 float t = -1;
		 float eps = 0.00000001f;
		 glm::vec3 v = ray->pos - p;
		 float A = glm::dot(ray->dir, ray->dir);
		 float B = 2.0f*glm::dot((v), ray->dir);
		 float C = glm::dot(v, v) - (r*r);
		 float Delta = (B*B) - (4.0f*A)*C;

		 if (Delta > 0)
		 {
			 t = -1000;
			
			 float t1 = (-B + sqrt(Delta)); 
			 t1 = t1 / (2.0f*A);
			 float t2 = (-B - sqrt(Delta));
			 t2=t2/ (2.0f*A);

			 if (t1 < t2)
			 {
				 t = t1;
				 if (t1 < eps)
				 {
					 t = t2;
					 if (t2 < eps)
					 {
						 t = -1000;
					 }
				 }
			 }
			 else
			 {
				 t = t2;
				 if (t2 < eps)
				 {
					 t = t1;
					 if (t1 < eps)
					 {
						 t = -1000;
					 }
				 }
			 }

		 }
		 else if (Delta< eps &&Delta>-eps)
		 {
			 float t1 = -B / 2.0f*A;
			 if (t1 > 0)
			 {
				 t = t1;
			 }
		 }
		 
		 return t;
	 }

	 CTriangle::CTriangle(glm::vec3 *t_verts, glm::vec3 t_amb, glm::vec3 t_diff, glm::vec3 t_spec, float shine, float refle)
	 {
		 type = OBJ_TRIANGLE;
		 vertices = t_verts;
		 ambient = t_amb;
		 diffuse = t_diff;
		 specular = t_spec;
		 shininess = shine;
		 reflect = refle;
		 refractionindex = 0.0f;

		 //calculate edges
		 glm::vec3 edge_v0v1 = vertices[1] - vertices[0];
		 glm::vec3 edge_v0v2 = vertices[2] - vertices[0];
		 //calculate Normal
		 normal = glm::cross(edge_v0v1, edge_v0v2);
		 normal = glm::normalize(normal);

	 }
	 glm::vec3 CTriangle::get_normal(glm::vec3 intersect) {
		
		 return normal;
	 }
	 void CTriangle::flip_normal() 
	 {
		 normal = -normal;
	 };
	 float CTriangle::intersect(CRay* ray) {
		 float t = -1;
		 

		 //check if ray and plane are parallel
		 float b = glm::dot(normal, ray->dir);
		 if (fabs(b) < 0.0001)
		 {
			 return t;//parallel don't intersect
		 }

		
		 glm::vec3 w0 = ray->pos - vertices[0];
		 float a = -glm::dot(normal,w0);
		
		 //calculate t
		 t = a / b;
		 //check if triangle is behind the ray
		 if (t < 0.0001)
		 {
			 return t;//triangle is behind don't intersect
		 }

		 //calculate intersection point
		 glm::vec3 p = ray->pos + t*ray->dir;

		 //Testing if ray hits inside triangle
		 /*
		 glm::vec3 C;
		 //edge 0 
		 glm::vec3 edge0= vertices[1] - vertices[0];
		 glm::vec3 vp0 = p- vertices[0];
		 C = glm::cross(edge0, vp0);
		 if (glm::dot(normal, C) < 0)
		 {
			 t = -1;
			 return t;
		 }
		 //edge 1
		 glm::vec3 edge1 = vertices[2] - vertices[1];
		 glm::vec3 vp1 = p - vertices[1];
		 C = glm::cross(edge1, vp1);
		 if (glm::dot(normal, C) < 0)
		 {
			 t = -1;
			 return t;
		 }
		 //edge 2
		 glm::vec3 edge2 = vertices[0] - vertices[2];
		 glm::vec3 vp2 = p - vertices[2];
		 C = glm::cross(edge2, vp2);
		 if (glm::dot(normal, C) < 0)
		 {
			 t = -1;
			 return t;
		 }
		 */

		 glm::vec3 v0 = vertices[2] - vertices[0];
		 glm::vec3 v1 = vertices[1] - vertices[0];
		 glm::vec3 v2 = p - vertices[0];

		 float dot00 = glm::dot(v0, v0);
		 float dot01 = glm::dot(v0, v1);
		 float dot02 = glm::dot(v0, v2);
		 float dot11 = glm::dot(v1, v1);
		 float dot12 = glm::dot(v1, v2);

		 float invDenom = 1 / (dot00*dot11 - dot01*dot01);
		 float u = (dot11*dot02 - dot01*dot12)*invDenom;
		 float v = (dot00*dot12 - dot01*dot02)*invDenom;

		 if (u > 0 && v > 0 && (u + v) < 1)
		 {
			 return t;
		 }
		 else
		 {
			 return -100;
		 }

		 


		 return t;
	 }