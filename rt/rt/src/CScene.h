
#include <vector>
/** Klasa opisujaca scene, definicje kamery, zrodel swiatla, obiektow, itp.
*/
class CScene {
public:
	CCamera cam; /**< kamera */

	std::vector<CObject*> obj;
	

	std::vector<CLight*> lights;
	

	/** Parsowanie pliku tektowego z informacjami o cenie.
	*	@param - nazwa pliku
	*/
	CScene() {};
	CScene(int c_width, int c_height, int c_fov, glm::vec3 c_eyep, glm::vec3 c_lookp, glm::vec3 c_up);
	void add_Sphere(float s_r, glm::vec3 s_p, glm::vec3 s_amb, glm::vec3 s_diff, glm::vec3 s_spec, float shine,float refle,float refraindex);
	void add_Triangle(glm::vec3 *t_verts, glm::vec3 t_amb, glm::vec3 t_diff, glm::vec3 t_spec, float shine, float refle,bool flip);
	void add_Light(glm::vec3 l_pos, glm::vec3 l_ambient, glm::vec3 l_diffuse, glm::vec3 l_specular);
	glm::vec3 ComputeLight(CObject *obj, CRay *ray,float t,float energy);
	
	
	int parse(char* fname);

};