
/** Wirtualna klasa definuj�ca obiekt
*/
class CObject {
public:
	int type; /**< rodzaj obiektu (OBJ_SPHERE, OBJ_TRIANGLE) */
	float reflect;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	float shininess;
	float refractionindex;

	CObject() {
	}

	/** Obliczenie przeci�cia promienia z obiektem */
	virtual float intersect(CRay* ray);
	/**Zwraca wektor normalny punktu przeciecia */
	virtual glm::vec3 get_normal(glm::vec3 intersect);
	virtual glm::vec3* get_vert() 
	{
		glm::vec3* test=new glm::vec3 [3];
		return test;
	}
	virtual void flip_normal();


};

/** Klasa opisujaca obiekt - kula
*/
class CSphere : public CObject {
public:
	float r; /**< promien */
	glm::vec3 p; /**< polozenie srodka */

	/**Konstruktor kuli */
	CSphere(float s_r, glm::vec3 s_p,glm::vec3 s_amb, glm::vec3 s_diff, glm::vec3 s_spec,float shine,float refle,float refraindex);

	/** Obliczenie przeci�cia promienia z kula */
	float intersect(CRay* ray);
	/**Zwraca wektor normalny punktu przeciecia */
	glm::vec3 get_normal(glm::vec3 intersect);
};

/** Klasa opisujaca obiekt - triangle
*/
class CTriangle : public CObject {
public:
	/*Wierzcho�ki tr�jk�ta*/
	glm::vec3 *vertices;
	glm::vec3 normal;
	/*Konstruktor tr�jkata*/
	CTriangle(glm::vec3 *t_verts, glm::vec3 t_amb, glm::vec3 t_diff, glm::vec3 t_spec, float shine, float refle);

	glm::vec3* get_vert() {
		return vertices;
	}
	/** Obliczenie przeci�cia promienia z tr�jk�tem */
	float intersect(CRay* ray);

	glm::vec3 get_normal(glm::vec3 intersect);
	void flip_normal();

};
