// rt.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "FreeImage.h"

using namespace std;

void main(int argc, char* argv[])
{
	FreeImage_Initialise;
	cout <<"FreeImage " <<FreeImage_GetVersion << endl;
	CRayTrace ray = CRayTrace(1000, 800,50,glm::vec3(3.0,15.0,-16.0),glm::vec3(0.0,0.0,0.0),glm::vec3(0.0,1.0,0.0));
	ray.run(&ray.scene,ray.image);
	FreeImage_DeInitialise;
	system("pause");
	return;
}

