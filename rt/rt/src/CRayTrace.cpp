#include "stdafx.h"
#include "FreeImage.h"

int CRayTrace::reflection(CRay &ray, COutput* res, CScene* scene, int intersect_index, float intersect_point)
{
	glm::vec3 p = ray.pos + intersect_point*ray.dir;
	glm::vec3 n = scene->obj[intersect_index]->get_normal(p);
	glm::vec3 v = ray.dir;
	glm::vec3 vprim = v - (glm::dot(2.0f*v, n))*n;

	raybounce++;
	CRay pix_ray_secondary = CRay(p+ vprim*0.001f, vprim);
	rayTrace(pix_ray_secondary, res, scene);
	return 0;
}
int CRayTrace::refraction(CRay &ray, COutput* res, CScene* scene,int intersect_index,float intersect_point,float refr)
{
	float EPS = 0.001f;
	float nrefr, n1, n2;
	glm::vec3 p = ray.pos + intersect_point*ray.dir;
	glm::vec3 n = scene->obj[intersect_index]->get_normal(p);
	glm::vec3 d = ray.dir;
	float cosI = glm::dot(d, n);
	if (cosI > 0.0f)
	{
		n1 = 1.0f;
		n2 = refr;
		n = -n;

	}
	else
	{
		n1 = refr;
		n2 = 1.0f;
		cosI = -cosI;
	}
	nrefr = n1 / n2;
	float cosT2 = 1.0f - powf(nrefr,2.0f)*(1.0f - powf(cosI,2.0f));
	if (cosT2 <0.0f)
	{
		CRay pix_ray_refr = CRay(p + n*EPS, n);
		rayTrace(pix_ray_refr, res, scene);
		raybounce++;
		return 0;
	}
	else if (cosT2 >0.0f)
	{
		glm::vec3 T = (nrefr*ray.dir) + (nrefr*cosI - sqrtf(cosT2))*n;
		CRay pix_ray_refr = CRay(p + T*EPS, T);
		rayTrace(pix_ray_refr, res, scene);
		raybounce++;
		return 0;
	}
	return 1;
}
int CRayTrace::rayTrace(CRay &ray, COutput* res,CScene* scene)
{
	float closest_intersect = std::numeric_limits<float>::max();
	float intersect_point = 0;;
	int intersect_index = 0;
	glm::vec3 col;
	for (unsigned int i = 0;i < scene->obj.size();i++)
	{
		
		if ((intersect_point =scene->obj[i]->intersect(&ray)) >0)
		{
			
			if (intersect_point < closest_intersect)
			{
				
				closest_intersect = intersect_point;
				intersect_index = i;
			}

			col = scene->ComputeLight(scene->obj[intersect_index], &ray, intersect_point, res->energy);


			res->color[0] += col.x;
			res->color[1] += col.y;
			res->color[2] += col.z;


			if (res->color[0]>1.0f)
			{
				res->color[0] = 0.99f;
			}
			else if (res->color[1] > 1.0f)
			{
				res->color[1] = 0.99f;
			}
			else if (res->color[2]>1.0f)
			{
				res->color[2] = 0.99f;
			}
			
			float refr = scene->obj[intersect_index]->refractionindex;

			if (refr > 0.1f&&raybounce<MaxBounce)
			{
				res->energy = res->energy*0.95;
				//refraction(ray, res, scene, intersect_index, intersect_point, refr);
			}

			if (scene->obj[intersect_index]->reflect>0.001f&&raybounce<MaxBounce)
			{
				res->energy = res->energy*0.5*scene->obj[intersect_index]->reflect;
				reflection(ray, res, scene, intersect_index, intersect_point);
			}

			
			
			
		}

	}


	return 0;
}
CRayTrace::CRayTrace(int width, int height, int c_fov, glm::vec3 c_eyep, glm::vec3 c_lookp, glm::vec3 c_up)
{
	image = CImage(width, height);
	scene = CScene(width, height, c_fov, c_eyep, c_lookp, c_up);
	MaxBounce = 10;
};

/** Glowna petla ray tracera
*/
int CRayTrace::run(CScene* scene, CImage& img) 
{
	scene->add_Sphere(2.0f, glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.8, 0.01, 0.01), glm::vec3(0.8, 0.8, 0.8),20.0f,0.0f,0.0f);
	scene->add_Sphere(1.0f, glm::vec3(2.0, 2.0, 0.0), glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.8, 0.01), glm::vec3(0.01, 0.01, 0.01),20.0f,0.0f,0.0f);
	//scene->add_Sphere(0.5f, glm::vec3(-1.0, -0.5, -7.0), glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.6, 0.01, 0.01), glm::vec3(0.01, 0.01, 0.01), 20.0f, 0.0f, 0.0f);
	//scene->add_Sphere(2.0f, glm::vec3(1.0, 1.5, 8.0), glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.2, 0.6), glm::vec3(0.01, 0.01, 0.01), 20.0f, 0.0f, 0.0f);
	//scene->add_Sphere(3.0f, glm::vec3(-5.0, 2.5, 7.0), glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.2, 0.3, 0.4), glm::vec3(0.01, 0.01, 0.01), 20.0f, 0.0f, 0.0f);
	//scene->add_Sphere(2.5f, glm::vec3(6.0, 2.15, -2.0), glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.3, 0.01, 0.3), glm::vec3(0.01, 0.01, 0.01), 120.0f, 2.0f, 0.0f);

	//scene->add_Light(glm::vec3(-5.0, 6.0, 0.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0));
	scene->add_Light(glm::vec3(1.0, 1.0, -10.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0));
	scene->add_Light(glm::vec3(-4.0, 4.0, -2.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0));

	//bottom
	glm::vec3 verts1[] = { glm::vec3(-5.0f, -4.0f, -5.0f) ,glm::vec3(-5.0f, -4.0f, 5.0f),glm::vec3(5.0f, -4.0f, 5.0f) };
	scene->add_Triangle(verts1, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.01, 0.3), glm::vec3(0.01, 0.01, 0.01), 20.0f, 1.0f,false);
	glm::vec3 verts2[] = { glm::vec3(5.0f, -4.0f, 5.0f), glm::vec3(5.0f, -4.0f, -5.0f),glm::vec3(-5.0f, -4.0f, -5.0f) };
	scene->add_Triangle(verts2, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.01, 0.3), glm::vec3(0.01, 0.01, 0.01), 20.0f, 1.0f,false);
	//right
	//glm::vec3 verts3[] = { glm::vec3(105.0f, -1.0f, 105.0f), glm::vec3(105.0f, 104.0f, 105.0f),glm::vec3(105.0f, -1.0f, -107.0f) };
	//scene->add_Triangle(verts3, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.4, 0.3), glm::vec3(0.01, 0.01, 0.01), 20.0f, 0.0f,false);
	//glm::vec3 verts10[] = { glm::vec3(105.0f, 104.0f, -107.0f), glm::vec3(105.0f, 104.0f, 105.0f),glm::vec3(105.0f, -1.0f, -107.0f) };
	//scene->add_Triangle(verts10, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.4, 0.3), glm::vec3(0.01, 0.01, 0.01), 20.0f, 0.0f, true);

	////left
	//glm::vec3 verts4[] = { glm::vec3(-105.0f, 104.0f, 105.0f), glm::vec3(-105.0f, -1.0f, 105.0f),glm::vec3(-105.0f, -1.0f, -107.0f) };
	//scene->add_Triangle(verts4, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.4, 0.3), glm::vec3(0.01, 0.01, 0.01), 20.0f, 0.0f,false);
	//glm::vec3 verts9[] = { glm::vec3(-105.0f, 104.0f, 105.0f), glm::vec3(-105.0f, 104.0f, -107.0f),glm::vec3(-105.0f, -1.0f, -107.0f) };
	//scene->add_Triangle(verts9, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.4, 0.3), glm::vec3(0.01, 0.01, 0.01), 20.0f, 0.0f, true);

	////back
	//glm::vec3 verts5[] = { glm::vec3(-105.0f, -1.0f, 105.0f), glm::vec3(-105.0f, 104.0f, 105.0f),glm::vec3(105.0f, -1.0f, 105.0f) };
	//scene->add_Triangle(verts5, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.4, 0.3), glm::vec3(0.01, 0.01, 0.01), 20.0f, 1.0f,false);
	//glm::vec3 verts6[] = { glm::vec3(-105.0f, 104.0f, 105.0f), glm::vec3(105.0f, 104.0f, 105.0f),glm::vec3(105.0f, -1.0f, 105.0f) };
	//scene->add_Triangle(verts6, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.4, 0.3), glm::vec3(0.01, 0.01, 0.01), 20.0f, 1.0f,false);
	////top
	//glm::vec3 verts7[] = { glm::vec3(-105.0f, 4.0f, -107.0f), glm::vec3(-105.0f, 4.0f, 105.0f),glm::vec3(105.0f, 4.0f, 105.0f) };
	//scene->add_Triangle(verts7, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.4, 0.01, 0.01), glm::vec3(0.01, 0.01, 0.01), 20.0f, 0.0f,false);
	//glm::vec3 verts8[] = { glm::vec3(105.0f, 4.0f, 105.0f), glm::vec3(105.0f, 4.0f, -107.0f),glm::vec3(-105.0f, 4.0f, -107.0f) };
	//scene->add_Triangle(verts8, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.4, 0.01, 0.01), glm::vec3(0.01, 0.01, 0.01), 20.0f, 0.0f,false);
	////front
	//glm::vec3 verts11[] = { glm::vec3(-105.0f, -1.0f, -107.0f), glm::vec3(-105.0f, 104.0f, -107.0f),glm::vec3(105.0f, -1.0f, -107.0f) };
	//scene->add_Triangle(verts11, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.4, 0.3), glm::vec3(0.01, 0.01, 0.01), 20.0f, 1.0f, false);
	//glm::vec3 verts12[] = { glm::vec3(-105.0f, 104.0f, -107.0f), glm::vec3(105.0f, 104.0f, -107.0f),glm::vec3(105.0f, -1.0f, -107.0f) };
	//scene->add_Triangle(verts12, glm::vec3(0.01, 0.01, 0.01), glm::vec3(0.01, 0.4, 0.3), glm::vec3(0.01, 0.01, 0.01), 20.0f, 1.0f, false);
	
	
	

	RGBQUAD color;
	
	glm::vec3 look =scene->cam.lookp- scene->cam.eyep;
	look = glm::normalize(look);
	glm::vec3 u = glm::cross(scene->cam.up, look);
	u = glm::normalize(u);
	glm::vec3 v = glm::cross(u, look);
	v = glm::normalize(v);
	glm::vec3 o = look / glm::length(look);
	o = o*(scene->cam.width / (2.0f * glm::tan(((glm::radians(scene->cam.fov / 2.0f) )))));
	o = o - ((scene->cam.width / 2.0f)*u);
	o =	o - ((scene->cam.height / 2.0f)*v);
	glm::mat3 matrix = glm::mat3(u, v, o);

	float apetureRadius = 0.25;
	float focalDistance = glm::length(glm::vec3(0.0, 1.0, 0.0) - scene->cam.eyep);
	int num_rays = 1;
	std::default_random_engine generator;
	std::normal_distribution<float> distribiution(0.0f, 1.0f);
	glm::vec3 xApertureRadius = u*apetureRadius;
	glm::vec3 yApertureRadius = v*apetureRadius;
	for (int y = 0; y < scene->cam.height; y++) {
		std::cout << "Y:" << y << std::endl;
		for (int x = 0; x < scene->cam.width; x++) {
			
			glm::vec3 final_color = glm::vec3(0.0f, 0.0f, 0.0f);
			glm::vec3 pixel = glm::vec3(x, y, 1.0f);
			glm::vec3 dir = matrix*pixel;
			dir = glm::normalize(dir);
			glm::vec3 aimedp = scene->cam.eyep+(focalDistance)*dir;
			
			
			for (int i = 0;i < num_rays;i++)
			{
				float r1 = distribiution(generator) ;
				float r2 = distribiution(generator) ;
				
				glm::vec3 start = scene->cam.eyep + r1*xApertureRadius + r2*yApertureRadius;
				glm::vec3 direction = aimedp - start;
				direction=glm::normalize(direction);
			
				raybounce = 0;
				
				//CRay pix_ray = CRay(start, direction);
				CRay pix_ray = CRay(scene->cam.eyep, dir);
				COutput out_col = COutput(1.0f, 0.0f, 0.0f, 0.0f);
				rayTrace(pix_ray, &out_col, scene);
				final_color.x += out_col.color[0];
				final_color.y += out_col.color[1];
				final_color.z += out_col.color[2];

			}
			final_color = final_color/ (float)num_rays;
			if (final_color.x>1.0f)
			{
				final_color.x = 0.99f;
			}
			else if (final_color.y > 1.0f)
			{
				final_color.y = 0.99f;
			}
			else if (final_color.z >1.0f)
			{
				final_color.z = 0.99f;
			}
			
			color.rgbRed = final_color.x *255;
			color.rgbGreen = final_color.y *255;
			color.rgbBlue = final_color.z *255;

			


			FreeImage_SetPixelColor(img.bitmap, x, y, &color);
		}
	}
	image.save("test");
	return 0;
}