
/** Definicja zrodla swiatla
*/
class CLight  {
public:
	glm::vec3 pos;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

	CLight(glm::vec3 l_pos, glm::vec3 l_ambient, glm::vec3 l_diffuse, glm::vec3 l_specular);
	
};
