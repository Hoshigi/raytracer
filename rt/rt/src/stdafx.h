
/** Typy obiektow
*/
#define OBJ_SPHERE 0
#define OBJ_TRIANGLE 1
#define OBJ_PLANE 2

#include <iostream>

#include "CCamera.h"
#include "CRay.h"
#include "CObject.h"
#include "CCamera.h"
#include "CLight.h"
#include "CScene.h"
#include "CImage.h"
#include "COutput.h"
#include "CRayTrace.h"
#include "glm.hpp"
#include "vec3.hpp"
#include <limits.h>
#include <random>