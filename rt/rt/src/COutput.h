
/** Dane przekazywane w procedurze rekurencyjnej
*/
class COutput {
public:
	float energy;
	int tree;
	float color[3];

	COutput(float o_energy, float red, float green, float blue);
	
};