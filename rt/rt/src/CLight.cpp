#include "stdafx.h"
CLight::CLight(glm::vec3 l_pos, glm::vec3 l_ambient, glm::vec3 l_diffuse, glm::vec3 l_specular)
{
	pos = l_pos;
	ambient = l_ambient;
	diffuse = l_diffuse;
	specular = l_specular;
}