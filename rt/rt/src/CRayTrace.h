

/** Glowna klasa ray tracera
 */
class CRayTrace {
public:
	CScene scene;
	CImage image;

	
public:
	int raybounce;
	int MaxBounce;
	CRayTrace(int width, int height, int c_fov, glm::vec3 c_eyep, glm::vec3 c_lookp, glm::vec3 c_up);
	int rayTrace(CRay &ray, COutput* res,CScene* scene);
	int refraction(CRay &ray, COutput* res, CScene* scene, int intersect_index, float intersect_point, float refr);
	int reflection(CRay &ray, COutput* res, CScene* scene, int intersect_index, float intersect_point);
	int run(CScene* scene, CImage& img);
	
};




