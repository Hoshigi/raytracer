#include "stdafx.h"


	CScene::CScene(int c_width, int c_height, int c_fov, glm::vec3 c_eyep, glm::vec3 c_lookp, glm::vec3 c_up)
	{
		cam = CCamera(c_width, c_height,c_fov,c_eyep,c_lookp,c_up);
		
	}
	void CScene::add_Sphere(float s_r, glm::vec3 s_p, glm::vec3 s_amb, glm::vec3 s_diff, glm::vec3 s_spec, float shine,float refle,float refraindex)
	{
		CObject *tmp =new  CSphere(s_r, s_p,s_amb,s_diff,s_spec,shine,refle,refraindex);
		obj.push_back(tmp);	
		
	}
	void CScene::add_Triangle(glm::vec3 *t_verts, glm::vec3 t_amb, glm::vec3 t_diff, glm::vec3 t_spec, float shine, float refle,bool flip)
	{
		
		CObject *tmp = new  CTriangle(t_verts, t_amb, t_diff, t_spec, shine, refle);
		if (flip)
		{
			tmp->flip_normal();
		}
		obj.push_back(tmp);
	}
	glm::vec3 CScene::ComputeLight(CObject*l_obj, CRay *ray,float t,float energy)
	{
		glm::vec3 i_total;
		glm::vec3 i_amb;
		glm::vec3 i_diff;
		glm::vec3 i_spec;
		
		i_amb = l_obj->ambient*lights[0]->ambient;
		i_total = i_amb*energy;

		for (int i = 0; i < lights.size(); i++)
		{
			bool shadowintersect = false;
			
			glm::vec3 p = ray->pos + t*ray->dir;
			glm::vec3 n = l_obj->get_normal(p);
			glm::vec3 l =  lights[i]->pos-p;
			float l_m = glm::length(l);
			l = glm::normalize(l);
			CRay *ShadowRay = new CRay(p, l);
			for (int j = 0;j < obj.size(); j++)
			{
				float shadow_hit = obj[j]->intersect(ShadowRay);
				if (shadow_hit >0.001f )
				{
					shadowintersect = true;
				}
			}
			delete ShadowRay;

			if (!shadowintersect)
			{
				
				float dot = glm::dot(l, n);
				if (dot > 0)
				{
					i_diff = l_obj->diffuse*lights[i]->diffuse*(dot);
					i_total = i_total + i_diff*energy;

					glm::vec3 v = -ray->dir;
					glm::vec3 r = 2.0f*glm::dot(n, l)*n - l;
					float dot2 = glm::dot(v, r);
					if (dot2 > 0)
					{
						i_spec = l_obj->specular*lights[i]->specular*pow(dot2, l_obj->shininess);
						i_total = i_total + i_spec*energy;
					}
				}
				
			}
		}
		return i_total;
	}

	int CScene::parse(char* fname) {

		return 0;
	}
	void CScene::add_Light(glm::vec3 l_pos, glm::vec3 l_ambient, glm::vec3 l_diffuse, glm::vec3 l_specular)
	{
		CLight *tmp = new CLight( l_pos,  l_ambient, l_diffuse, l_specular);
		lights.push_back(tmp);

	}
