#include "glm.hpp"
#ifndef _CCAMERA_H_
#define _CCAMERA_H_

/** Klasa opisujaca parametry kamery
*/
class CCamera {
public:
	int width;
	int height;
	float fov;
	glm::vec3 eyep;
	glm::vec3 lookp;
	glm::vec3 up;

public:
	CCamera() {};
	CCamera(int c_width, int c_height,int c_fov, glm::vec3 c_eyep, glm::vec3 c_lookp, glm::vec3 c_up);

};

#endif