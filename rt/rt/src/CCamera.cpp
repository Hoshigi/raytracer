#include "stdafx.h"
CCamera::CCamera(int c_width, int c_height, int c_fov, glm::vec3 c_eyep, glm::vec3 c_lookp, glm::vec3 c_up)
{
	width = c_width;
	height = c_height;
	fov = c_fov;
	eyep = c_eyep;
	lookp = c_lookp;
	up = c_up;
};